## [1.0.9] - 2021-11-29
### Add
- interface for user configuration
- user_id fill with qr auth for multiuser
## [1.0.8] - 2021-11-23
### Add
- Delete user handler
## [1.0.7] - 2021-11-22
- Hotfix of social login
## [1.0.2] - 2021-11-18
### Fixed
- Samples refs
## [1.0.1] - 2021-11-18
- Update patterns package
### Add
- Avatar Setup Method
## [1.0.0] - 2021-11-18
- Update dependencies
- Refactoring
### Removed
- Assets Api Interface
## [0.0.7] - 2021-10-22
### Added
- Special types of avatars data
- GetFromExtension method in ModelsUrl interface
## [0.0.6] - 2021-10-21
- Update dependencies
- Prepare package
## [0.0.5] - 2021-10-19
### Added
- Special types of avatars models
- Sample
### Bug Fixes
- Empty item list
